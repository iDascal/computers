from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url('20', views.twenty, name='twenty'),
    url('24', views.twenty_four, name='twenty_four'),
    url('58', views.fifty_eight, name='fifty_eight'),
    url('85', views.eighty_five, name='eighty_five'),
]