from django.contrib import admin

from .models import Product, PC, Laptop, Printer

admin.site.register(Product)
admin.site.register(Printer)
admin.site.register(PC)
admin.site.register(Laptop)

# Register your models here.
