from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from .models import PC, Product

def index(request):
    result = 'WTF?'
    template = loader.get_template('tasks/index.html')
    context = RequestContext(request, {
        'result': result
        })
    return HttpResponse(template.render(context))

def twenty(request):
    task = 'Find the makers producing at least three distinct models of PCs. Result set: maker, number of PC models.'

    pc_units = Product.objects.filter(product_type__exact = 'PC')

    collection = {}
    for unit in pc_units:
        collection.setdefault('{}'.format(unit.maker), []).append(unit.product_type)

    result = [(k, len(v)) for k, v in collection.items() if len(v) > 2]
    # print('========================================================================')
    # print(collection)
    # result = pc_units[0].model.maker
    # result = collection

    template = loader.get_template('tasks/result.html')
    context = RequestContext(request, {
        'task': task,
        'result': result
        })
    return HttpResponse(template.render(context))

def twenty_four(request):
    return HttpResponse('24')

def fifty_eight(request):
    return HttpResponse('58')

def eighty_five(request):
    return HttpResponse('85')



# Create your views here.
