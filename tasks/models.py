from django.db import models

class Product(models.Model):
    maker = models.CharField(max_length=10)
    model = models.CharField(max_length=50, primary_key=True)
    product_type = models.CharField(max_length=50)

    class Meta:
        db_table = 'Product'


class PC(models.Model):
    code = models.IntegerField(primary_key=True)
    model = models.ForeignKey(Product)
    speed = models.SmallIntegerField()
    ram = models.SmallIntegerField()
    hd = models.IntegerField()
    cd = models.CharField(max_length=10)
    price = models.DecimalField(max_digits=999, decimal_places=2)

    class Meta:
        db_table = 'PC'

    def __str__(self):
        return('{}'.format(self.code))

class Laptop(models.Model):
    code = models.IntegerField(primary_key=True)
    model = models.ForeignKey(Product)
    speed = models.SmallIntegerField()
    ram = models.SmallIntegerField()
    hd = models.IntegerField()
    price = models.DecimalField(max_digits=999, decimal_places=2)
    screen = models.SmallIntegerField()

    class Meta:
        db_table = 'Laptop'


class Printer(models.Model):
    code = models.IntegerField(primary_key=True)
    model = models.ForeignKey(Product)
    color = models.CharField(max_length=1)
    printer_type = models.CharField(max_length=10)
    price = models.DecimalField(max_digits=999, decimal_places=2)

    class Meta:
        db_table = 'Printer'


