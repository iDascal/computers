# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Laptop',
            fields=[
                ('code', models.IntegerField(serialize=False, primary_key=True)),
                ('speed', models.SmallIntegerField()),
                ('ram', models.SmallIntegerField()),
                ('hd', models.IntegerField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=999)),
                ('screen', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'Laptop',
            },
        ),
        migrations.CreateModel(
            name='PC',
            fields=[
                ('code', models.IntegerField(serialize=False, primary_key=True)),
                ('speed', models.SmallIntegerField()),
                ('ram', models.SmallIntegerField()),
                ('hd', models.IntegerField()),
                ('cd', models.CharField(max_length=10)),
                ('price', models.DecimalField(decimal_places=2, max_digits=999)),
            ],
            options={
                'db_table': 'PC',
            },
        ),
        migrations.CreateModel(
            name='Printer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('code', models.IntegerField(unique=True)),
                ('color', models.CharField(max_length=1)),
                ('printer_type', models.CharField(max_length=10)),
                ('price', models.DecimalField(decimal_places=2, max_digits=999)),
            ],
            options={
                'db_table': 'Printer',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('maker', models.CharField(max_length=10)),
                ('model', models.CharField(max_length=50, serialize=False, primary_key=True)),
                ('product_type', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'Product',
            },
        ),
        migrations.AddField(
            model_name='printer',
            name='model',
            field=models.ForeignKey(to='tasks.Product'),
        ),
        migrations.AddField(
            model_name='pc',
            name='model',
            field=models.ForeignKey(to='tasks.Product'),
        ),
        migrations.AddField(
            model_name='laptop',
            name='model',
            field=models.ForeignKey(to='tasks.Product'),
        ),
    ]
